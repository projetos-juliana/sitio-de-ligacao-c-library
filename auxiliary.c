/* 
 * File:   auxiliary.c
 * Author: felth
 *
 * Created on 8 de Fevereiro de 2021, 15:59
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "basics.h"

//Pair functions

pair_r* allocate_pair() {
    pair_r* p = (pair_r*) malloc(sizeof (pair_r));
    p->r1 = 0;
    p->r2 = 0;
    return p;
}

pair_r* allocate_n_pairs(int n) {
    pair_r* p = (pair_r*) malloc(n * sizeof (pair_r));
    for (int i = 0; i < n; i++) {
        p[i].r1 = 0;
        p[i].r2 = 0;
    }
    return p;
}

const char* print_pair(pair_r* local_pair_r) {
    if (local_pair_r != NULL) {
        char s[55];
        sprintf(s, "Pair: R1 = %-0.4g R2 = %-0.4g", local_pair_r->r1, local_pair_r->r2);
        const char* s_out = s;
        return s_out;
    } else {
        return "NULL";
    }
}

void test_change_pair(pair_r* local_pair_r) {
    if (local_pair_r != NULL) {
        local_pair_r->r1 *= 2;
        local_pair_r->r2 *= 3;
    }
}

void deallocate_pair(pair_r* local_pair_r) {
    free(local_pair_r);
}

//Map Theta R1R2 functions

map_theta_r1r2* allocate_map_theta_r1r2() {
    map_theta_r1r2* m = (map_theta_r1r2*) malloc(sizeof (map_theta_r1r2));
    m->theta = (double*) malloc(MAX_ANGLE * sizeof (double));
    memset(m->theta, 0, sizeof (double) * MAX_ANGLE);
    for (int i = 0; i < MAX_ANGLE; i++) {
        m->theta[i] = i;
    }
    m->value = allocate_n_pairs(MAX_ANGLE);
    return m;
}

const char* print_map_theta_r1r2_element(const map_theta_r1r2* local_map_theta_r1r2, int pos) {
    char t[66];
    sprintf(t, "%3d ->[%3g] => R1 = %-0.14g R2 = %-0.14g\n",
            pos,
            local_map_theta_r1r2->theta[pos],
            local_map_theta_r1r2->value[pos].r1,
            local_map_theta_r1r2->value[pos].r2);
    const char* t_out = t;
    return t_out;
}

const char* print_map_theta_r1r2(const map_theta_r1r2* local_map_theta_r1r2) {
    if (local_map_theta_r1r2 != NULL) {
        char s[66 * 180 + 7] = "Mapa:\n";
        for (int i = 0; i < 180; i++) {
            strcat(s, print_map_theta_r1r2_element(local_map_theta_r1r2, i));
        }
        const char* s_out = s;
        return s_out;
    } else {
        return "NULL";
    }
}

void deallocate_map_theta_r1r2(map_theta_r1r2* local_map_theta_r1r2) {
    free(local_map_theta_r1r2->theta);
    free(local_map_theta_r1r2->value);
    free(local_map_theta_r1r2);
}

//Experimento functions

experimento* allocate_experimento(const char* tipo) {
    experimento* e = (experimento*) malloc(sizeof (experimento));
    e->tipo = tipo;
    e->constante_sv = 0;
    e->fluorescencia = 0;
    return e;
}

const char* print_experimento(experimento* local_experimento) {
    if (local_experimento != NULL) {
        char s[110];
        sprintf(s, "Experimento %s => Constante Stern-Volmer = %-0.14g Fluorescencia = %-0.14g", local_experimento->tipo, local_experimento->constante_sv, local_experimento->fluorescencia);
        const char* s_out = s;
        return s_out;
    } else {
        return "NULL";
    }
}

void test_change_experimento(experimento* local_experimento) {
    if (local_experimento != NULL) {
        local_experimento->constante_sv *= 2;
        local_experimento->fluorescencia *= 3;
    }
}

void deallocate_experimento(experimento* local_experimento) {
    free(local_experimento);
}

//Modelo functions

const char* print_modelo(modelo* local_modelo) {
    if (local_modelo != NULL) {
        char s[2 * 110 + 66 * 180 + 7 + 22 + 30];
        const char* e0 = print_experimento(local_modelo->experimento_bsa);
        const char* e1 = print_experimento(local_modelo->experimento_hsa);
        const char* m = print_map_theta_r1r2(local_modelo->mapa_r1r2);

        sprintf(s, "Modelo:\n%s\n%s\nRazao R1/R2 = %-0.14g D = %-0.14g\n%s", e0, e1, local_modelo->razao_r1r2, local_modelo->d, m);
//        printf("%s %-0.14g %-0.14g", local_modelo->experimento_bsa->tipo, local_modelo->experimento_bsa->constante_sv, local_modelo->experimento_bsa->fluorescencia);
//        printf("%s %-0.14g %-0.14g", local_modelo->experimento_hsa->tipo, local_modelo->experimento_hsa->constante_sv, local_modelo->experimento_hsa->fluorescencia);
        const char* s_out = s;
        return s_out;
    } else {
        return "NULL";
    }
}
