#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=MinGW_64-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/MinGW_64-Windows
CND_ARTIFACT_NAME_Debug=libsitio-de-ligacao-c-library.dll
CND_ARTIFACT_PATH_Debug=dist/Debug/MinGW_64-Windows/libsitio-de-ligacao-c-library.dll
CND_PACKAGE_DIR_Debug=dist/Debug/MinGW_64-Windows/package
CND_PACKAGE_NAME_Debug=libsitio-de-ligacao-c-library.dll.tar
CND_PACKAGE_PATH_Debug=dist/Debug/MinGW_64-Windows/package/libsitio-de-ligacao-c-library.dll.tar
# Release configuration
CND_PLATFORM_Release=MinGW-Windows
CND_ARTIFACT_DIR_Release=dist/Release/MinGW-Windows
CND_ARTIFACT_NAME_Release=libsitio-de-ligacao-c-library.a
CND_ARTIFACT_PATH_Release=dist/Release/MinGW-Windows/libsitio-de-ligacao-c-library.a
CND_PACKAGE_DIR_Release=dist/Release/MinGW-Windows/package
CND_PACKAGE_NAME_Release=sitio-de-ligacao-c-library.tar
CND_PACKAGE_PATH_Release=dist/Release/MinGW-Windows/package/sitio-de-ligacao-c-library.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
