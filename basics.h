/* 
 * File:   basics.h
 * Author: felth
 *
 * Created on 4 de Fevereiro de 2021, 00:04
 */

#ifndef BASICS_H
#define BASICS_H

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_ANGLE 180

    typedef struct pair_r {
        double r1;
        double r2;
    } pair_r;

    typedef struct map_theta_r1r2 {
        double *theta;
        struct pair_r *value;
    } map_theta_r1r2;

    typedef struct experimento {
        char *tipo;
        double constante_sv;
        double fluorescencia;
    } experimento;

    typedef struct modelo {
        double razao_r1r2;
        double d;
        map_theta_r1r2 *mapa_r1r2;
        experimento *experimento_bsa;
        experimento *experimento_hsa;
    } modelo;

    //Pair Functions Declaration
    pair_r* allocate_pair();
    pair_r* allocate_n_pairs();
    const char* print_pair(pair_r* local_pair_r);
    void test_change_pair(pair_r* local_pair_r);
    void deallocate_pair(pair_r* local_pair_r);
    //Map Theta R1R2 Functions Declaration
    map_theta_r1r2* allocate_map_theta_r1r2();
    const char* print_map_theta_r1r2_element(const map_theta_r1r2* local_map_theta_r1r2, int pos);
    const char* print_map_theta_r1r2(const map_theta_r1r2* local_pair_r);
    void deallocate_map_theta_r1r2(map_theta_r1r2* local_pair_r);
    //Experimento Functions Declaration
    experimento* allocate_experimento(const char* tipo);
    const char* print_experimento(experimento* local_experimento);
    void test_change_experimento(experimento* local_experimento);
    void deallocate_experimento(experimento* local_experimento);
    //Modelo Functions Declaration
    const char* print_modelo(modelo* local_modelo);
    

    //Main Functions Declaration
    const char* get_version();
    void desalocar_modelo(modelo* local_modelo);
    modelo* alocar_modelo();

#ifdef __cplusplus
}
#endif

#endif /* BASICS_H */

