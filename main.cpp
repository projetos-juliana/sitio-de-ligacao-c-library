/* 
 * File:   main.cpp
 * Author: felth
 *
 * Created on 3 de Fevereiro de 2021, 23:06
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "basics.h"

#define VERSION "0.3.1"

/*
 * 
 */

modelo* g_modelo;

const char* get_version() {
    return VERSION;
}

void desalocar_modelo(modelo* local_modelo) {
    deallocate_experimento(local_modelo->experimento_bsa);
    deallocate_experimento(local_modelo->experimento_hsa);
    deallocate_map_theta_r1r2(local_modelo->mapa_r1r2);
    free(local_modelo);
//    free(g_modelo);
}

modelo* alocar_modelo() {
    if (g_modelo) {
        desalocar_modelo(g_modelo);
    }
    modelo* local_model;
    g_modelo = (modelo*) malloc(sizeof (modelo));
    local_model = g_modelo;

    local_model->razao_r1r2 = 0;
    local_model->d = 1;
    local_model->mapa_r1r2 = allocate_map_theta_r1r2();
    local_model->experimento_bsa = allocate_experimento("BSA");
    local_model->experimento_hsa = allocate_experimento("HSA");
    return local_model;
}

modelo* iniciar_modelo() {
    alocar_modelo();
    strcpy(g_modelo->experimento_bsa->tipo, "BSA");
    strcpy(g_modelo->experimento_hsa->tipo, "HSA");
    return g_modelo;
}

void setExperimento(char* tipo, double constanteSV, double fluorescencia) {
    if (g_modelo) {
        if (strcmpi(tipo, "BSA")) {
            g_modelo->experimento_bsa->constante_sv = constanteSV;
            g_modelo->experimento_bsa->fluorescencia = fluorescencia;
        } else if (strcmpi(tipo, "HSA")) {
            g_modelo->experimento_hsa->constante_sv = constanteSV;
            g_modelo->experimento_hsa->fluorescencia = fluorescencia;
        }
    }
}

void setD(double D) {
    if (g_modelo) {
        g_modelo->d = D;
    }
}

bool possuiCondicoesValidas() {
    double Kb = g_modelo->experimento_bsa->constante_sv;
    double Kh = g_modelo->experimento_hsa->constante_sv;
    double Fb = g_modelo->experimento_bsa->fluorescencia;
    double Fh = g_modelo->experimento_hsa->fluorescencia;

    return ((Kb * Fb) / (Kh * Fh)) - 1 >= 0;
}

double determinarRazao() {
    double Kb = g_modelo->experimento_bsa->constante_sv;
    double Kh = g_modelo->experimento_hsa->constante_sv;
    double Fb = g_modelo->experimento_bsa->fluorescencia;
    double Fh = g_modelo->experimento_hsa->fluorescencia;

    g_modelo->razao_r1r2 = (pow(((Kb * Fb) / (Kh * Fh)) - 1, 1.0 / 6.0));
    return g_modelo->razao_r1r2;
}

pair_r determinar_R2(double angulo) {
    pair_r retorno;
    if (angulo != 0.0) {
        double a = 1 - pow(g_modelo->razao_r1r2, 2);
        double b = 2 * g_modelo->d * g_modelo->razao_r1r2 * cos(angulo);
        double c = -pow(g_modelo->d, 2);
        double delta = pow(b, 2) - (4 * a * c);

        if (delta >= 0) {
            retorno.r2 = (-(b) + sqrt(delta)) / (2 * a);

            if (retorno.r2 < 0) {
                retorno.r2 = (-(b) - sqrt(delta)) / (2 * a);
            }
        }
    } else {
        retorno.r2 = g_modelo->d / (1 + g_modelo->razao_r1r2);
    }
    retorno.r1 = g_modelo->razao_r1r2 * retorno.r2;
    return retorno;
}

pair_r determinar_R2_alternativo(double angulo) {
    double a = pow(g_modelo->razao_r1r2, 2) + 1 - (2 * g_modelo->razao_r1r2 * cos(angulo));
    double b = 0.0;
    double c = -pow(g_modelo->d, 2);
    double delta = pow(b, 2) - (4 * a * c);
    pair_r retorno;

    if (delta >= 0) {
        retorno.r2 = (-(b) + sqrt(delta)) / (2 * a);

        if (retorno.r2 < 0) {
            retorno.r2 = (-(b) - sqrt(delta)) / (2 * a);
        }
    }
    retorno.r1 = g_modelo->razao_r1r2 * retorno.r2;
    return retorno;
}

void construirResultados() {
    int i = 0;
    for (double theta = 0; theta < M_PI; theta += (180 / M_PI)) {
        pair_r r1R2 = determinar_R2(theta);
        g_modelo->mapa_r1r2->theta[i] = theta;
        g_modelo->mapa_r1r2->value[i++] = r1R2;
    }
}

void construirResultadosAlternativo() {
    int i = 0;
    for (double theta = 0; theta < M_PI; theta += (180 / M_PI)) {
        pair_r r1R2 = determinar_R2_alternativo(theta);
        g_modelo->mapa_r1r2->theta[i] = theta;
        g_modelo->mapa_r1r2->value[i++] = r1R2;
    }
}

void extrapolacao() {
    determinarRazao();
    construirResultados();
}

map_theta_r1r2* getMapaR1R2() {
    return (g_modelo->mapa_r1r2);
}
